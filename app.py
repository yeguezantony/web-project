from flask import Flask, request, render_template, redirect, session, url_for
from flask_sqlalchemy import SQLAlchemy
import bcrypt

app = Flask(__name__)
app.secret_key = 'your_secret_key'  # Cambia 'secret_key' por una clave segura
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///prueba.db'
db = SQLAlchemy(app)

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(200), nullable=False)
    lastname = db.Column(db.String(200), nullable=False)
    email = db.Column(db.String(200), nullable=False)
    password = db.Column(db.String(200))

    def __init__(self, username, lastname, email, password):
        self.username = username
        self.lastname = lastname
        self.email = email
        self.password = bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt()).decode('utf-8')
    
    def check_password(self, password):
        return bcrypt.checkpw(password.encode('utf-8'), self.password.encode('utf-8'))

with app.app_context():
    db.create_all()

@app.route('/')
def index():
    if 'username' in session:  # Verifica si el usuario está autenticado
        return redirect(url_for('logueado'))
    else:
        return render_template('index.html')

@app.route('/register',methods=['GET','POST'])
def register():
    if request.method == 'POST':
        # handle request
        username = request.form['username']
        lastname = request.form['lastname']
        email = request.form['email']
        password = request.form['password']

        new_user = User(username=username,lastname=lastname,email=email,password=password)
        db.session.add(new_user)
        db.session.commit()
        return redirect('/login')
    
    return render_template('register.html')

@app.route('/login',methods=['GET','POST'])
def login():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']

        user = User.query.filter_by(username=username).first()
        
        if user and user.check_password(password):
            session['username'] = user.username
            return redirect('/logueado')
        
    return render_template('login.html')

@app.route('/logout')
def logout():
    session.pop('username',None)
    return render_template('index.html')
            

@app.route('/remedies')
def remedies():
    if 'username' in session:  # Verifica si el usuario está autenticado
        return render_template('remedies/remedies.html')
    else:
        return render_template('index.html')

@app.route('/remedies/first_article')
def first_article():
    if 'username' in session:  # Verifica si el usuario está autenticado
        return render_template('remedies/first_article/Causes_of_Headache.html')
    else:
        return render_template('index.html')

@app.route('/remedies/second_article')
def second_article():
    if 'username' in session:  # Verifica si el usuario está autenticado
        return render_template('remedies/second_article/Drinking_water.html')
    else:
        return render_template('index.html')

@app.route('/remedies/third_article')
def third_article():
    if 'username' in session:  # Verifica si el usuario está autenticado
        return render_template('remedies/third_article/Taking_magnesium.html')
    else:
        return render_template('index.html')

@app.route('/remedies/four_article')
def four_article():
    if 'username' in session:  # Verifica si el usuario está autenticado
        return render_template('remedies/four_article/Get_enough_sleep.html')
    else:
        return render_template('index.html')

@app.route('/about')
def about():
    if 'username' in session:  # Verifica si el usuario está autenticado
        return render_template('about/about.html')
    else:
        return render_template('index.html')
    
@app.route('/contact')
def contact():
    if 'username' in session:  # Verifica si el usuario está autenticado
        return render_template('contact/contact.html')
    else:
        return render_template('index.html')
    
@app.route('/logueado')
def logueado():
    if 'username' in session:  # Verifica si el usuario está autenticado
        return render_template('logueado/logueado.html')
    else:
        return render_template('index.html')

@app.route('/second_page')  # Endpoint actualizado a 'second_page'
def second_page():
    if 'username' in session:  # Verifica si el usuario está autenticado
        return render_template('second_page/second_page.html')
    else:
        return render_template('index.html')

@app.route('/second_page/first_article2')
def first_article2():
    if 'username' in session:  # Verifica si el usuario está autenticado
        return render_template('second_page/first_article2/Try_taking_vitamin_B.html')
    else:
        return render_template('index.html')
    
@app.route('/second_page/second_article2')
def second_article2():
    if 'username' in session:  # Verifica si el usuario está autenticado
        return render_template('second_page/second_article2/Try_an_elimination_diet.html')
    else:
        return render_template('index.html')

@app.route('/second_page/third_article2')
def third_article2():
    if 'username' in session:  # Verifica si el usuario está autenticado
        return render_template('second_page/third_article2/Drink_caffeinated.html')
    else:
        return render_template('index.html')
    
@app.route('/second_page/four_article2')
def four_article2():
    if 'username' in session:  # Verifica si el usuario está autenticado
        return render_template('second_page/four_article2/Avoid_strong_odors.html')
    else:
        return render_template('index.html')
 
@app.route('/third_page')  # Corregido el nombre del endpoint a 'third_page'
def third_page():
    if 'username' in session:  # Verifica si el usuario está autenticado
        return render_template('third_page/third_page.html')
    else:
        return render_template('index.html')
    
@app.route('/third_page/first_article3')
def first_article3():
    if 'username' in session:  # Verifica si el usuario está autenticado
        return render_template('third_page/first_article3/Try_herbal_remedies.html')
    else:
        return render_template('index.html')
    
@app.route('/third_page/second_article3')
def second_article3():
    if 'username' in session:  # Verifica si el usuario está autenticado
        return render_template('third_page/second_article3/Avoid_nitrates_and_nitrites.html')
    else:
        return render_template('index.html')

@app.route('/third_page/third_article3')
def third_article3():
    if 'username' in session:  # Verifica si el usuario está autenticado
        return render_template('third_page/third_article3/Drink_ginger_tea.html')
    else:
        return render_template('index.html')

@app.route('/third_page/four_article3')
def four_article3():
    if 'username' in session:  # Verifica si el usuario está autenticado
        return render_template('third_page/four_article3/Exercise.html')
    else:
        return render_template('index.html')

if __name__ == '__main__':
    app.run(debug=True)

    